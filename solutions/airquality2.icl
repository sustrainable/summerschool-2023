module airquality2
/**
 * Programs for the Sustrainable 2023 summer school, Coimbra, Portugal
 *
 * Date: July 2023
 * Authors: Mart Lubbers & Pieter Koopman (mart@cs.ru.nl, pieter@cs.ru.nl)
 */

import StdEnv, iTasks, Data.Tuple
import mTask.Interpret
import mTask.Interpret.Device.TCP
import Device

Start :: !*World -> *World
Start w = doTasks (main <<@ ApplyLayout frameCompact) w

airqualityShareI = sharedStore "airquality" 400

main :: Task ()
main =         enterDeviceInfo
	>>? \spec->withDevice spec deviceTask
	>>* [ OnAction (Action "Stop") (always (shutDown 0))
	    , OnAction (Action "Reset") (always main)
		]
where
	deviceTask :: MTDevice -> Task ()
	deviceTask dev =
			liftmTask airqualitymTask dev
		-|| viewSharedInformation [] airqualityShareI
			<<@ Label "Airquality eCO2"

airqualitymTask :: Main (MTask v ()) | mtask, lowerSds, dht, NeoPixel, AirQualitySensor v
airqualitymTask =
	airqualitySensor airqualitySensorWemosSGP30Shield \aqs->
	dht dhtWemosSHT30Shield \dht->
	neopixel neopixelWemosRGBLEDShield \neo->
	lowerSds \airqualityShareM=airqualityShareI
	In fun \calibrate=(\(oldtemp, oldhumid)->
		     temperature dht .&&. humidity dht
		>>*. [IfValue (tupopen \(t, h)->differsTH (t, h) (oldtemp, oldhumid)) rtrn]
		>>=. tupopen \(t, h)->setEnvironmentalData aqs t h
		>>|. setPixelColor neo (lit 1) (lit 50) (lit 50) (lit 50)
		>>|. delay (ms 200)
		>>|. setPixelColor neo (lit 1) (lit 0) (lit 0) (lit 0)
		>>|. calibrate (t, h)
	) In fun \measureAirquality=(\old->
		     co2` (BeforeSec (lit 1)) aqs
		>>*. [IfValue (\x->differs 50 old x) (\nv->setSds airqualityShareM nv)]
		>>=. \nv->delay (ms 200)
		>>|. measureAirquality nv
	) In {main=
		         temperature dht
		>>~. \t->humidity dht
		>>~. \h->setEnvironmentalData aqs t h
		>>|.     getSds airqualityShareM
		>>~. \v->measureAirquality (lit 0) .||. calibrate (t, h)
	}
where
	differsTH (newT, newH) (oldT, oldH)
		=  differs 1.0  newT oldT
		|. differs 10.0 newH oldH

	differs eps l r = If (l >. r) (l -. r >. lit eps) (r -. l >. lit eps)
