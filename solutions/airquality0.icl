module airquality0
/**
 * Programs for the Sustrainable 2023 summer school, Coimbra, Portugal
 *
 * Date: July 2023
 * Authors: Mart Lubbers & Pieter Koopman (mart@cs.ru.nl, pieter@cs.ru.nl)
 */

import StdEnv, iTasks, Data.Tuple
import mTask.Interpret
import mTask.Interpret.Device.TCP
import Device

Start :: !*World -> *World
Start w = doTasks (main <<@ ApplyLayout frameCompact) w

airqualityShareI = sharedStore "airquality" 400

main :: Task ()
main =         enterDeviceInfo
	>>? \spec->withDevice spec deviceTask
	>>* [ OnAction (Action "Stop") (always (shutDown 0))
	    , OnAction (Action "Reset") (always main)
	    ]
where
	deviceTask :: MTDevice -> Task ()
	deviceTask dev =
		    liftmTask airqualitymTask dev
		-|| viewSharedInformation [] airqualityShareI
			<<@ Label "Airquality eCO2"

airqualitymTask :: Main (MTask v ()) | mtask, lowerSds, dht, NeoPixel, AirQualitySensor v
airqualitymTask =
	airqualitySensor airqualitySensorWemosSGP30Shield \aqs->
	dht dhtWemosSHT30Shield \dht->
	neopixel neopixelWemosRGBLEDShield \neo->
	lowerSds \airqualityShareM=airqualityShareI
	In fun \measureAirquality=(\old->
		     co2` (BeforeSec (lit 1)) aqs
		>>*. [IfValue (\x->differs 50 old x) (\nv->setSds airqualityShareM nv)]
		>>=. \nv->delay (ms 200)
		>>|. measureAirquality nv
	) In {main=
		measureAirquality (lit 0)
	}
where
	differs :: Int (v Int) (v Int) -> v Bool | mtask v
	differs eps old new =
		If (old >. new)
			(old -. new >. lit eps)
			(new -. old >. lit eps)
