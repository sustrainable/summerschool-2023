module helloworld
/**
 * Programs for the Sustrainable 2023 summer school, Coimbra, Portugal
 *
 * Date: July 2023
 * Authors: Mart Lubbers & Pieter Koopman (mart@cs.ru.nl, pieter@cs.ru.nl)
 */

import StdEnv, iTasks
import mTask.Interpret, mTask.Interpret.Device.TCP
import Device

Start :: !*World -> *World
Start w = doTasks (main <<@ ApplyLayout frameCompact) w/*\label{lst:hw:doTasks}*/

main :: Task ()
main =         enterDeviceInfo/*\label{lst:hw:enter}*/
	>>? \spec->withDevice spec deviceTask/*\label{lst:hw:wd}*/
	>>* [ OnAction (Action "Stop") (always (shutDown 0))/*\label{lst:hw:stop}*/
	    , OnAction (Action "Reset") (always main)
		]
where
	deviceTask :: MTDevice -> Task ()
	deviceTask dev = liftmTask blink dev/*\label{lst:hw:lmt}*/

blink :: Main (MTask v ()) | mtask, NeoPixel v
blink = neopixel neopixelWemosRGBLEDShield \neo-> /*\label{lst:hw:def}*/
	fun \b2i=(\b->If b (lit 10) (lit 0))/*\label{lst:hw:help}*/
	In fun \blinkfun=(\st-> /*\label{lst:hw:fun}*/
		     setPixelColor neo (lit 0) (b2i st) (b2i st) (b2i st)/*\label{lst:hw:sp}*/
		>>|. delay (ms 500)/*\label{lst:hw:delay}*/
		>>|. blinkfun (Not st)/*\label{lst:hw:rec}*/
	) In {main=blinkfun true}/*\label{lst:hw:main}*/
