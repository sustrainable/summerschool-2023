module airquality
/**
 * Programs for the Sustrainable 2023 summer school, Coimbra, Portugal
 *
 * Date: July 2023
 * Authors: Mart Lubbers & Pieter Koopman (mart@cs.ru.nl, pieter@cs.ru.nl)
 */

import StdEnv, iTasks, Data.Tuple
import mTask.Interpret
import mTask.Interpret.Device.TCP
import Device

Start :: !*World -> *World
Start w = doTasks main w

airqualityShareI = sharedStore "airquality" 400

main :: Task ()
main =         enterDeviceInfo
	>>? \spec->withDevice spec deviceTask
	>>* [ OnAction (Action "Stop") (always (return ()))
	    , OnAction (Action "Reset") (always main)
		]
where
	deviceTask :: MTDevice -> Task ()
	deviceTask dev =
			liftmTask airqualitymTask dev
		-|| viewSharedInformation [] airqualityShareI
			<<@ Label "Airquality eCO2"

airqualitymTask :: Main (MTask v ()) | mtask, lowerSds, dht, AirQualitySensor v
airqualitymTask =
	airqualitySensor airqualitySensorWemosSGP30Shield \aqs->
	dht dhtWemosSHT30Shield \dht->
	lowerSds \airqualityShareM=airqualityShareI
	In fun \differs=(\(old, new)->
		If (old >. new)
			(old -. new >. lit 100)
			(new -. old >. lit 100)
	) In fun \measureAirquality=(\old->
		     co2` (BeforeSec (lit 1)) aqs
		>>*. [IfValue (\x->differs (old, x)) (\nv->setSds airqualityShareM nv)]
		>>=. \nv->measureAirquality nv
	) In {main=
		/*         setEnvFromDHT aqs dht
		>>..     getSds airqualityShareM
		>>~. \v->*/measureAirquality (lit 0)}
