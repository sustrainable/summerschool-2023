module walking
/**
 * Programs for the Sustrainable 2023 summer school, Coimbra, Portugal
 *
 * Your first mTask program that blinks the LED on the LED shield.
 *
 * Date: July 2023
 * Authors: Mart Lubbers & Pieter Koopman (mart@cs.ru.nl, pieter@cs.ru.nl)
 */
import StdEnv, iTasks
import mTask.Interpret
import mTask.Interpret.Device.TCP
import Device

Start w = doTasks main w

main :: Task ()
main =         enterDeviceInfo
	>>? \spec->withDevice spec deviceTask
	>>* [ OnAction (Action "Stop") (always (return ()))
	    , OnAction (Action "Reset") (always main)
		]
where
	deviceTask :: MTDevice -> Task ()
	deviceTask dev = liftmTask walk dev

walk :: Main (MTask v ()) | mtask, NeoPixel v
walk = neopixel neopixelWemosRGBLEDShield \neo->
	fun \blinkonce=(\i->
		     setPixelColor neo i level level level
		>>|. delay delta
		>>|. setPixelColor neo i off off off
	) In fun \walkfun=(\i->If (i ==. lit 7) (walkfun (lit 0))
		(blinkonce i >>|. walkfun (i +. lit 1))
	) In {main=walkfun (lit 0)}

off = lit 0
level = lit 10
delta = ms 500
