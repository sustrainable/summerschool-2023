implementation module Device

import StdEnv
import iTasks, iTasks.Extensions.Files
import mTask.Interpret
import mTask.Interpret.Device.TCP

enterDeviceInfo :: Task TCPSettings
enterDeviceInfo
	= catchAll (
		 updateSharedInformation [] (removeMaybe (?Just defaultDeviceInfo) (sdsFocus "device.json" jsonFileShare))
		<<@ Hint "Enter device settings below (copy from OLED screen)"
	// Extra fields are ignored, but when the json is ill-formed (e.g. a field
	// has the wrong type) an exception is thrown.
	) (\e->viewInformation [] "Error reading device.json file, please delete it" <<@ Title e
		>>* [ OnAction ActionContinue (always (return ()))
		    , OnAction (Action "Delete for me") (always (deleteFile "device.json"))
			]
		>-| enterDeviceInfo
	)

defaultDeviceInfo :: TCPSettings
defaultDeviceInfo = {TCPSettings | host="192.168.1.1", port=8123, pingTimeout= ?None}

runMTask :: ((Main (BCInterpret (TaskValue u)))) ->Task () | type u
runMTask mTask = enterDeviceInfo
	>>? \dev->withDevice dev (liftmTask mTask)
	>>* [ OnAction (Action "Stop") (always (return ()))
	    , OnAction (Action "Reset") (always (runMTask mTask))
		]
