module temperature
/**
 * Programs for the Sustrainable 2023 summer school, Coimbra, Portugal
 *
 * Date: July 2023
 * Authors: Mart Lubbers & Pieter Koopman (mart@cs.ru.nl, pieter@cs.ru.nl)
 */
import StdEnv, iTasks, Data.Tuple
import mTask.Interpret
import mTask.Interpret.Device.TCP
import Device

Start :: !*World -> *World
Start w = doTasks combinedTask w

tSDS :: SimpleSDSLens Real
tSDS = sharedStore "tempSDS" -273.15 // brrr​

combinedTask :: Task ()
combinedTask =
  runMTask tempMTask -||
  (Label "Temperature" @>> viewSharedInformation [] tSDS)

tempMTask :: (Main (MTask v Long)) | mtask, lowerSds, dht v
tempMTask =
	dht dhtWemosSHT30Shield \sensor->
	lowerSds \rSDS = tSDS In
	{main = rpeat (
		 temperature sensor >>~. \t.
		 setSds rSDS t >>|.
		 delay (ms 200))
	}
