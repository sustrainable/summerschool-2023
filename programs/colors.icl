module colors
/**
 * Programs for the Sustrainable 2023 summer school, Coimbra, Portugal
 *
 * Date: July 2023
 * Authors: Mart Lubbers & Pieter Koopman (mart@cs.ru.nl, pieter@cs.ru.nl)
 */
import StdEnv, iTasks, Data.Tuple
import mTask.Interpret
import mTask.Interpret.Device.TCP
import Device

Start :: !*World -> *World
Start w = doTasks main w

redShareI = sharedStore "red" 0
grnShareI = sharedStore "grn" 0
bluShareI = sharedStore "blu" 0

main :: Task ()
main =         enterDeviceInfo
	>>? \spec->withDevice spec deviceTask
	>>* [ OnAction (Action "Stop") (always (return ()))
	    , OnAction (Action "Reset") (always main)
		]
where
	deviceTask :: MTDevice -> Task ()
	deviceTask dev = liftmTask (color 0) dev
		-|| (updateSharedInformation [] redShareI <<@ Label "Red")
		-|| (updateSharedInformation [] grnShareI <<@ Label "Green")
		-|| (updateSharedInformation [] bluShareI <<@ Label "Blue")

color :: Int -> Main (MTask v ()) | mtask, lowerSds, NeoPixel, AirQualitySensor v
color lednumber = neopixel neopixelWemosRGBLEDShield \neo->
	   lowerSds \red=redShareI
	In lowerSds \grn=grnShareI
	In lowerSds \blu=bluShareI
	In {main=rpeat (
		           getSds red .&&. (getSds grn .&&. getSds blu)
		>>~. \clr->setPixelColor neo (lit lednumber)
			(first   clr)
			(first  (second clr))
			(second (second clr))
	)}
