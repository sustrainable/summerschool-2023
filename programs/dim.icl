module dim
/**
 * Programs for the Sustrainable 2023 summer school, Coimbra, Portugal
 *
 * Date: July 2023
 * Authors: Mart Lubbers & Pieter Koopman (mart@cs.ru.nl, pieter@cs.ru.nl)
 */
import StdEnv, iTasks, Data.Tuple
import mTask.Interpret
import mTask.Interpret.Device.TCP
import Device

Start :: !*World -> *World
Start w = doTasks combinedTask w

dimShareI = sharedStore "dim" 0

combinedTask :: Task ()
combinedTask = (updateSharedInformation [] dimShareI <<@ Label "Value between 0 and 255") ||- runMTask (color 0)

color :: Int -> Main (MTask v ()) | mtask, lowerSds, NeoPixel, AirQualitySensor v
color lednumber =
	neopixel neopixelWemosRGBLEDShield \neo->
	lowerSds \dim = dimShareI In
	{main = rpeat (getSds dim
		           >>~. \d->setPixelColor neo (lit lednumber) d d d)
	}
