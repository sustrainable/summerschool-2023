definition module Device

from iTasks.WF.Definition import :: Task
from mTask.Interpret.Device.TCP import :: TCPSettings
from mTask.Language.Types import :: Main, class type
import mTask.Interpret.DSL

/**
 * Task that asks the user for the device settings. The device settings are
 * read from a device.json file so that changes are persistent. If this file
 * does not exist, it is created.
 */
enterDeviceInfo :: Task TCPSettings

runMTask :: ((Main (BCInterpret (TaskValue u)))) ->Task () | type u
