# SusTrainable Summer School 2023

This repository contains the material for the lecture _Sustainable IoT programming_ [SusTrainable](https://sustrainable.github.io/) [summer school 2023](https://sustrainable.dei.uc.pt/) held between 10 and 14 July, 2023 at the University of Coimbra, Portugal.

## Contents

- `./programs/`: the solutions to the exercises
- `./skeletons/`: the skeleton programs for the exercises
- `./slides/`: the lecture slides
- `./lecturenotes/`: the lecture notes/exercise reader

## Authors

- Pieter Koopman (pieter@cs.ru.nl)
- Mart Lubbers (mart@cs.ru.nl)
