module temperature
/**
 * Programs for the Sustrainable 2023 summer school, Coimbra, Portugal
 *
 * Date: July 2023
 * Authors: Mart Lubbers & Pieter Koopman (mart@cs.ru.nl, pieter@cs.ru.nl)
 */

import StdEnv, iTasks, Data.Tuple
import mTask.Interpret
import mTask.Interpret.Device.TCP
import Device

Start :: !*World -> *World
Start w = doTasks (main <<@ ApplyLayout frameCompact) w

highTempI = sharedStore "highTemp" 25.0
lowTempI = sharedStore "lowTemp" 18.0
curTempI = sharedStore "curTemp" 20.0

main :: Task ()
main =         enterDeviceInfo
	>>? \spec->withDevice spec deviceTask
	>>* [ OnAction (Action "Stop") (always (shutDown 0))
	    , OnAction (Action "Reset") (always main)
		]
where
	deviceTask :: MTDevice -> Task ()
	deviceTask dev =
			liftmTask temperatureTask dev
		-|| (updateSharedInformation [] highTempI <<@ Label "Upper limit")
		-|| (updateSharedInformation [] lowTempI  <<@ Label "Lower limit")
		-|| (viewSharedInformation   [] curTempI  <<@ Label "Current temperature °C")

temperatureTask :: Main (MTask v ()) | mtask, lowerSds, dht, NeoPixel v
temperatureTask =
	neopixel neopixelWemosRGBLEDShield \neo->
	dht dhtWemosSHT30Shield \dht->
	   lowerSds \highTempM=highTempI
	In lowerSds \lowTempM=lowTempI
	In lowerSds \curTempM=curTempI
	In fun \setColor=(\x->
		         getSds lowTempM
		>>~. \l->getSds highTempM
		>>~. \h->setPixelColor neo (lit 0)
			(/* Set the red color component */)
			(/* Set the green color component */)
			(/* Set the blue color component */)
	) In fun \measureTemp=(\old->
		     temperature dht
		>>~. \x->setSds curTempM x
		>>|. setColor x
		>>|. delay (ms 250)
		>>|. measureTemp x
	) In {main=measureTemp (lit 0.0)}
