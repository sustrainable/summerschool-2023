module walking
/**
 * Programs for the Sustrainable 2023 summer school, Coimbra, Portugal
 *
 * Date: July 2023
 * Authors: Mart Lubbers & Pieter Koopman (mart@cs.ru.nl, pieter@cs.ru.nl)
 */
import StdEnv, iTasks
import mTask.Interpret, mTask.Interpret.Device.TCP
import Device

Start :: !*World -> *World
Start w = doTasks (main <<@ ApplyLayout frameCompact) w

main :: Task ()
main =         enterDeviceInfo
	>>? \spec->withDevice spec deviceTask
	>>* [ OnAction (Action "Stop") (always (shutDown 0))
	    , OnAction (Action "Reset") (always main)
		]
where
	deviceTask :: MTDevice -> Task ()
	deviceTask dev = liftmTask walk dev

walk :: Main (MTask v ()) | mtask, NeoPixel v
walk = neopixel neopixelWemosRGBLEDShield \neo->
	fun \blinkonce=(\i->
		     /* turn the pixel on */
		>>|. delay (ms 1000)
		>>|. /* turn the pixel off */
	) In fun \walkfun=(\i->
		/* Loop through the LEDs here */
		If (i ==. lit 7)
			/* Loop back to zero */
			/* Blink the LED once and recursively call walkfun with the increment*/
	) In {main=walkfun (lit 0)}
