module helloworld
/**
 * Programs for the Sustrainable 2023 summer school, Coimbra, Portugal
 *
 * Date: July 2023
 * Authors: Mart Lubbers & Pieter Koopman (mart@cs.ru.nl, pieter@cs.ru.nl)
 */

import StdEnv, iTasks
import mTask.Interpret, mTask.Interpret.Device.TCP
import Device

Start :: !*World -> *World
Start w = doTasks (main <<@ ApplyLayout frameCompact) w

main :: Task ()
main =         enterDeviceInfo
	>>? \spec->withDevice spec deviceTask
	>>* [ OnAction (Action "Stop") (always (shutDown 0))
	    , OnAction (Action "Reset") (always main)
		]
where
	deviceTask :: MTDevice -> Task ()
	deviceTask dev = liftmTask blink dev

blink :: Main (MTask v ()) | mtask, NeoPixel v
blink = neopixel neopixelWemosRGBLEDShield \neo-> 
	fun \b2i=(\b->If b (lit 10) (lit 0))
	In fun \blinkfun=(\st-> 
		     setPixelColor neo (lit 0) (b2i st) (b2i st) (b2i st)
		>>|. delay (ms 500)
		>>|. blinkfun (Not st)
	) In {main=blinkfun true}
