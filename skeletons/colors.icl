module colors
/**
 * Programs for the Sustrainable 2023 summer school, Coimbra, Portugal
 *
 * Date: July 2023
 * Authors: Mart Lubbers & Pieter Koopman (mart@cs.ru.nl, pieter@cs.ru.nl)
 */
import StdEnv, iTasks
import mTask.Interpret, mTask.Interpret.Device.TCP
import Device

Start :: !*World -> *World
Start w = doTasks (main <<@ ApplyLayout frameCompact) w

redShareI = sharedStore "red" 0
bluShareI = sharedStore "blu" 0
grnShareI = sharedStore "grn" 0

main :: Task ()
main =         enterDeviceInfo
	>>? \spec->withDevice spec deviceTask
	>>* [ OnAction (Action "Stop") (always (shutDown 0))
	    , OnAction (Action "Reset") (always main)
		]
where
	deviceTask :: MTDevice -> Task ()
	deviceTask dev = liftmTask (color 0) dev
		-|| updateSharedInformation [UpdateSharedUsing id (const id) colorSliderEditor]
			(redShareI >*< bluShareI >*< grnShareI)

color :: Int -> Main (MTask v ()) | mtask, lowerSds, NeoPixel, AirQualitySensor v
color lednumber = neopixel neopixelWemosRGBLEDShield \neo->
	   lowerSds \red=redShareI
	In /* lower the green share here */
	In /* lower the blue share here */
	In {main=rpeat (
		         delay (ms 100)
		>>|.     getSds red
		>>~. \r-> /* retrieve the value of the green share here */
		>>~. \g-> /* retrieve the value of the green share here */
		>>~. \b->setPixelColor neo (lit lednumber) r g b
	)}
