implementation module Device

import StdEnv
import Data.Func, Data.Tuple
import iTasks, iTasks.Extensions.Files
import mTask.Interpret
import mTask.Interpret.Device.TCP

enterDeviceInfo :: Task TCPSettings
enterDeviceInfo
	= catchAll (
		 updateSharedInformation [] (removeMaybe (?Just defaultDeviceInfo) (sdsFocus "device.json" jsonFileShare))
		<<@ Hint "Enter device settings below (copy from OLED screen)"
	// Extra fields are ignored, but when the json is ill-formed (e.g. a field
	// has the wrong type) an exception is thrown.
	) (\e->viewInformation [] "Error reading device.json file, please delete it" <<@ Title e
		>>* [ OnAction ActionContinue (always (return ()))
		    , OnAction (Action "Delete for me") (always (deleteFile "device.json"))
			]
		>-| enterDeviceInfo
	)

defaultDeviceInfo :: TCPSettings
defaultDeviceInfo = {TCPSettings | host="192.168.1.1", port=8123, pingTimeout= ?None}

colorSliderEditor :: Editor ((Int, Int), Int) (EditorReport ((Int, Int), Int))
colorSliderEditor = mapEditorWrite (editorReportTuple2 o appFst editorReportTuple2)
	$ panel2
		(panel2 (sliderComponent "Red") (sliderComponent "Blue"))
		(sliderComponent "Green")

sliderComponent :: !String -> Editor Int (EditorReport Int)
sliderComponent lbl = mapEditorRead (\x->(x, x))
	$ mapEditorWrite (ValidEditor o snd)
	$ panel2
		(mapEditorRead (toString) label <<@ Label lbl)
		(slider <<@ minAttr 0 <<@ maxAttr 255)
