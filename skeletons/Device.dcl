definition module Device

from iTasks.UI.Editor import :: Editor, :: EditorReport
from iTasks.WF.Definition import :: Task
from mTask.Interpret.Device.TCP import :: TCPSettings

/**
 * Task that asks the user for the device settings. The device settings are
 * read from a device.json file so that changes are persistent. If this file
 * does not exist, it is created.
 */
enterDeviceInfo :: Task TCPSettings

/**
 * An Editor for a color component
 */
colorSliderEditor :: Editor ((Int, Int), Int) (EditorReport ((Int, Int), Int))
