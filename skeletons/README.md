# Sustainable IoT programming software preparation

For this practical, we use the Clean programming language that comes with a
package manager (nitrile). Please, at least follow the _Setup instructions_
before coming to the practical.

If there are any questions, don't hesitate to contact Mart Lubbers, either on
slack or by sending an email to mart@cs.ru.nl.

## Setup instructions

Instructions come from [clean-lang.org](https://clean-lang.org/about.html#install).

### Windows

- Open an elevated powershell terminal
- Install chocolateley (https://chocolatey.org/install)
- _RESTART YOUR POWERSHELL TERMINAL_
- Run the following command: `Set-ExecutionPolicy ByPass -Scope Process -Force; (New-Object System.Net.WebClient).DownloadString('https://clean-lang.org/install.ps1') | powershell -Command -`
- _REBOOT YOUR SYSTEM_
- If you now run `nitrile` in your powershell terminal you should see the help output.

### Linux

- Install `curl`, `jq`, `tar` and `libz3-4` (debian: `sudo apt-get install curl jq tar libz3-4`)
- Install `nitrile` by running: `curl https://clean-lang.org/install.sh | /bin/sh`
- Add `~/.nitrile/bin` to your path (e.g. for `bash` by running: `echo 'export PATH="$PATH:~/nitrile/bin"' >> ~/.bashrc`)
- If you now run `nitrile` in your terminal, you should see the help text

### Mac OS using VS code and devcontainers (also an option for windows and linux)

- Find the section for your OS on this page: https://gitlab.com/clean-and-itasks/itasks-template/-/blob/main/README.md
- Follow the instructions from here to set up VS code _UP TO BUT NOT INCLUDING STEP 4_ (the one where you clone the repo).
- Open the current folder (where you stored this readme) in VS code
- A message should pop up offering to open the repository within a development container, do so.
- After the development container is built, verify that everything is working
  by opening a terminal within VSCode and running:
  - `nitrile update`
  - `nitrile fetch`
- If anything does not work as expected, it is recommended to rebuild the
  container, click the "Dev container" button in the bottom left of VSCode and
  select "Rebuild container".

## Compile and run mTask for the first time:

- Open this directory in your terminal (linux) powershell/cmd (windows) or in a VS code devcontainer terminal (mac and other).
- To fetch the latest package versions from the registry, run: `nitrile update`
- To fetch the actual dependencies, run: `nitrile fetch`
- To build the example progam, run: `nitrile build --only=helloworld`
  NB. this initial compilation will also compile all libraries and can take some minutes.
- Now there should be a `helloworld` (linux and devcontainer) `helloworld.exe` (windows) executable in the directory in conjunction with a `helloworld-www` folder, a `helloworld.bc` file and a `helloworld.pbc` file

## Compile and run a skeleton for the *n*th time where *n > 1*

- Open the directory in your terminal (linux) powershell (windows) or in a VS code devcontainer (mac and other).
- Run: `nitrile build --only=NAME`
- for some `NAME.icl` file in the directory.

## Tools to support you when developing Clean/iTask/mTask applications

- Visual studio code plugin for Clean (VS code wil probably prompt you to install it automatically when you open a Clean file for the first time) ([available in the marketplace](https://marketplace.visualstudio.com/items?itemName=TOPSoftware.clean-vs-code))
- Vim plugin for Clean ([https://gitlab.com/clean-and-itasks/vim-clean](https://gitlab.com/clean-and-itasks/vim-clean))
- [cloogle.org](cloogle.org): A search engine for Clean functions (includes iTask and mTask)
