\documentclass[runningheads]{llncs}

\usepackage[utf8]{inputenc} % Allow for easier diacritics

\usepackage[obeyspaces,hyphens]{url} % URL typesetting
\urlstyle{tt}

\usepackage{graphicx} % Images
\graphicspath{{./img}}

\usepackage[hidelinks]{hyperref} % Hyperlinks
\newcommand{\lstnumberautorefname}{Line}

\usepackage[british]{babel} % English internationalisation
\usepackage[nodayofweek]{datetime} % Format dates according to the locale

\usepackage{siunitx} % units
\DeclareSIUnit\celcius{{}^{\circ}\kern-\scriptspace\mathsf{C}}

\usepackage{booktabs} % Nice tables
\usepackage{textcomp} % Nice upquote (')
\usepackage{stmaryrd} % Short arrows
\usepackage{listings} % Source code formatting

\usepackage{lstlangpowershell} % Powershell listings definition
\usepackage{lstlangclean} % Clean listings definition
\usepackage{lstlangarduino} % Arduino C++ listings definition

\usepackage{xcolor} % backgroundcolor
\definecolor{lstbg}{gray}{.95}

% General listings setup
\lstset{%
	backgroundcolor=\color{lstbg},
	basewidth=0.5em,
	basicstyle=\footnotesize\tt,
	breakatwhitespace=false,
	breaklines=true,
	captionpos=b,
	columns=[c]fixed,
	commentstyle=\sl,
	escapeinside={/*}{*/},  % chktex 9
	keepspaces=true,
	keywordstyle=\bf,
	postbreak=\mbox{\textcolor{gray}{$\hookrightarrow$}\space},
	showspaces=false,
	showstringspaces=false,
	showtabs=false,
	stringstyle=\it,
	tabsize=4,
	texcl=true,
	inputpath={../solutions},
}

\newcommand{\cleaninline}[1]{\lstinline[language=Clean]{#1}}

% Assignments
\usepackage{float}
\floatstyle{ruled}
\newfloat{exercise}{ht}{loe}
\floatname{exercise}{Exercise}
\renewcommand{\theexercise}{\arabic{exercise}}
\newenvironment{anexercise}[2]{\begin{exercise}\caption{#1\hfill\footnotesize file: \url{#2.icl}}}{\end{exercise}}
\newfloat{solution}{ht}{los}
\floatname{solution}{Solution}
\renewcommand{\thesolution}{\arabic{solution}}
\newenvironment{asolution}[1]{\begin{solution}\caption{#1}}{\end{solution}}
\newenvironment{hint}{{\par\bf Hint: }\em\/}{}

% Remove or set to disable when submitting
\usepackage{todonotes} % Todos and comments
\newcommand{\pietercomment}[1]{\todo[color=blue!20]{#1}}
\newcommand{\martcomment}[1]{\todo[color=green!20]{#1}}

\newcommand{\Cpp}[0]{C\texttt{++}}
\newcommand{\C}[0]{C}
\newcommand{\CCpp}[0]{\C/\Cpp{}}
\newcommand{\Clean}[0]{Clean}
\newcommand{\mTask}[0]{mTask}
\newcommand{\MTask}[0]{MTask}
\newcommand{\iTask}[0]{iTask}
\newcommand{\ITask}[0]{ITask}
\newcommand{\IIC}[0]{I\textsuperscript{2}C}

\title{Sustainable Internet of Things Computing}
\author{%
	Mart Lubbers\orcidID{0000-0002-4015-4878}
	\and
	Pieter Koopman\orcidID{0000-0002-3688-0957}
}
\date{July 2023}
\institute{%
	Institute for Computing and Information Sciences,\\
	Radboud University, Nijmegen, The Netherlands\\
	\email{firstname@cs.ru.nl}
}

\begin{document}

\maketitle

%\begin{abstract}
%	\ldots
%\end{abstract}

\section{Exercises}
\subsection{Hello world!}
The first program anyone writes when trying a new programming language is the so-called \emph{Hello World!} program.
While the program only prints \emph{Hello World!} to the screen and exits, it allows you to verify that the toolchain is working.
Typical \mTask{} devices, e.g.\ microcontrollers, only have a very simple \numproduct{1 x 1} monochrome screen, the built in {LED}. %chktex 29
Therefore, the best we can do is let the world know that the program is working by turning this screen on or off.
On the other hand, \mTask{}'s toolchain is a lot more elaborate.

First the Clean program representing the \iTask{} application is compiled.
During the execution of the resulting program, \mTask{} devices may be connected.
Once connected, an mTask is compiled at runtime to a specialised byte code that is sent to the device dynamically.
In turn, the device interprets the byte code to create a task tree, a runtime representation of a task-oriented program.
Step by step, the task tree is rewritten, yielding an observable value at every step.
This observable value, and information about the used shared data sources (SDSs) is communicated with the server.

Listing~\ref{lst:helloworldI} shows the complete \iTask{} part of the \emph{Hello World!} program.
After the module heading and the imports, line~\ref{lst:hw:doTasks} contains the standard way of starting an \iTask{} engine with task \cleaninline{main} as the argument.
The \cleaninline{main} task shows the main \iTask{} task.
First, the device information is asked from the user (line~\ref{lst:hw:enter}).
With this information, \cleaninline{withDevice} is used to connect the device (line~\ref{lst:hw:wd}).
The second argument of the \cleaninline{withDevice} function, the body, just lifts the \cleaninline{blink} \mTask{} task to an \iTask{} task (line~\ref{lst:hw:lmt}).
This sets the machinery in motion described above and results in the \mTask{} device executing the program.
To allow the user to prematurely end the program (and gracefully disconnect the device), line~\ref{lst:hw:stop} uses the \cleaninline{>>*} combinator to add a button to the user interface to terminate the task.

\lstinputlisting[language=Clean,linerange={1-1,8-24},numbers=left,caption={\emph{Hello World!}, the \iTask{} part},label={lst:helloworldI}]{helloworld.icl}%chktex 8

The \cleaninline{blink} task shown in listing~\ref{lst:helloworldM} first defines the NeoPixel peripheral on line~\ref{lst:hw:def}.
Then a helper function is defined that converts a boolean to a suitable light intensity value (line~\ref{lst:hw:help}).
The light intensity can range from \numrange{0}{255}\footnotemark.
\footnotetext{Remember to put on sunglasses before ramping the brightness to $255$.}
The lines \numrange[parse-numbers=false]{\ref{lst:hw:fun}}{\ref{lst:hw:rec}} define the blinking function.
The blinking function takes one argument, the current state and performs several actions combined using the sequence operator (\cleaninline{>>|}).
Using this argument it first sets the first pixel on the shield to the correct value using the \cleaninline{b2i} helper (line~\ref{lst:hw:sp}).
Then it waits for \qty{500}{\ms} (see line~\ref{lst:hw:delay}).
Finally, it calls itself recursively with the inverse of the state (line~\ref{lst:hw:rec}) to achieve the blinking behaviour.
The \cleaninline{main} record denotes the main expression, in here, the \cleaninline{blinkfun} is simply called.
The \cleaninline{lit} function transforms a Clean value to a literal in mTask.

\lstinputlisting[language=Clean,firstnumber=19,linerange={26-33},numbers=left,caption={\emph{Hello World!}, the \mTask{} part},label={lst:helloworldM}]{helloworld.icl}%chktex 8

\begin{anexercise}{Hello World!}{helloworld}\label{ex:helloworld}
	Compile and run your first \mTask{} program (see \url{README.html} for instructions on how to install Clean, \iTask{} and \mTask{}).

	On Linux or through a visual studio code \emph{devcontainer} you run in a shell:

\begin{lstlisting}[language=bash]
nitrile build --only=helloworld
./helloworld
\end{lstlisting}

	On windows you run in a PowerShell:

\begin{lstlisting}[language=bash]
nitrile build --only=helloworld
.\helloworld
\end{lstlisting}

	Once your program is up and running, navigate to \url{localhost:8080} to see the \iTask{} interface.
	Here you enter the IP address shown on the OLED screen of the device in the \emph{Host: } field.
	Leave the \emph{Port} and \emph{Ping timeout} fields for what they are.
	When you have pressed continue, the middle LED of the RGB LED shield should blink to tell you hello!

	\hint{Try changing the frequency or position of the LED.}
\end{anexercise}

Use \url{cloogle.org} to lookup  details about functions in Clean, iTask and mTask.

\subsection{Colors}
The \mTask{} system integrates with \iTask{} using only three integration functions, simplified types of these are shown in listing~\ref{lst:integration}.
\cleaninline{withDevice} is used to connect an \mTask{} device to an \iTask{} server so that tasks can be executed.
\cleaninline{liftmTask} is used to compile, send and execute a task on a device, it lifts an \mTask{} task to an \iTask{} task.
Finally, \cleaninline{lowerSds} is used to connect \iTask{} SDSs to \mTask{} SDSs.

\begin{lstlisting}[language=Clean,caption={Integration functions of \mTask{} with \iTask{}.},label={lst:integration}]
withDevice :: TCPSettings (MTDevice -> Task b) -> Task b | ...
liftmTask :: (Main (MTask BCInterpret u)) MTDevice -> Task u | ...
lowerSds :: ((v (Sds t))->In (Shared sds t) (Main (MTask v u)))
	-> Main (MTask v u) | ...
\end{lstlisting}

While tasks have an observable task value, some collaboration patterns benefit a lot from the \emph{many-to-many} communication SDSs offer.
As SDSs from \iTask{} can be accessed by \mTask{}, the powerful web editor infrastructure of \iTask{} can be used to communicate with \mTask{} tasks.

\begin{anexercise}{Colors}{colors}
	Use \cleaninline{lowerSds} to access the color values that are stored in the \iTask{} {SDS}.
	See the lowering of \cleaninline{redShareI} for an example.

	Then read the SDSs in \mTask{} and provide the values to \cleaninline{setPixelColor}.

	\hint{The \cleaninline{>>~.} combinator is used because \cleaninline{getSds} always yields an \emph{unstable} value.}
\end{anexercise}

\subsection{Walking}
As seen in listing~\ref{lst:neopixel}, the interface to the NeoPixel peripheral only contains one function besides the constructor.
This function, \cleaninline{setPixelColor} requires five arguments.
The first argument is the handle to the peripheral, obtained by defining it using the constructor.
The second argument is the index of the LED you want to address.
The RGB LED shield contains seven LEDs so indices \numrange{0}{6} are valid here.
The other arguments are the RGB color components.

\begin{lstlisting}[language=Clean,caption={NeoPixel \mTask{} interface.},label={lst:neopixel}]
class NeoPixel v where
	neopixel      :: NeoInfo ((v NeoPixel) -> Main (v b)) -> Main (v b)
	setPixelColor :: (v NeoPixel) (v Int) (v Int) (v Int) (v Int)
		-> MTask v ()
\end{lstlisting}

\begin{anexercise}{Walking}{walking}
	Fill in the gaps in the \cleaninline{walk} \mTask{} task so that instead of blinking one LED, it it walks through the full range of LEDs.

	First create \cleaninline{blinkonce} \mTask{} function by inserting the correct \cleaninline{setPixelColor} calls.

	Then implement the \cleaninline{walkfun} \mTask{} function that iterates over the LEDs and calls \cleaninline{blinkonce} for each one.

	\hint{There are \num{7} LEDs on the board so the valid indices range from \numrange{0}{6}.}
\end{anexercise}

\subsection{Sensors}
Attached to the microcontroller is a digital temperature and humidity (DHT) sensor, sht30x, that connects via \IIC{} to the board.
The interface is shown in listing~\ref{lst:dht}.
The constructor (\cleaninline{dht}) works very similar to the NeoPixel constructor.
\cleaninline{temperature} yields the temperature as an unstable value in \unit{\celcius}.
\cleaninline{humidity} yields the relative humidity as an unstable value in \unit{\%}.

\begin{lstlisting}[language=Clean,caption={DHT \mTask{} interface.},label={lst:dht}]
class dht v where
	dht :: DHTInfo ((v DHT)->Main (v b)) -> Main (v b)
	temperature :: (v DHT) -> MTask v Real
	humidity :: (v DHT) -> MTask v Real
\end{lstlisting}

\begin{anexercise}{Temperature}{temperature}
	The program consists of two functions.
	\cleaninline{measureTemp} measures the temperature and sets the color of the pixel according to the limits.
	\cleaninline{setColor} reads the lowered SDSs and determines the color of the pixel.
	Blue if it is too cold, green if it's within limits and red if it's too hot.
	Implement the logic for setting the pixel colors and observe the behaviour\footnotemark.

	\hint{Use the conditional statement \cleaninline{If} and one of the comparison operations to calculate the color.
		In \mTask{}, all arithmetic operations are suffixed with a \cleaninline{.} (a period), e.g. \cleaninline{+.}, \cleaninline{-.}, \cleaninline{*.}, \cleaninline{/.}, \cleaninline{>.}, \cleaninline{ <.}, \cleaninline{>=.}, \cleaninline{<=.}.
		The \emph{if}-expression is written with the \cleaninline{If} function.
		Furthermore, literals must always be lifted so $100$ in \mTask{} is written as \cleaninline{lit 100}.
	}
\end{anexercise}
\footnotetext{If you look for a challenge, you can also implement a gradient.}

The \textsc{sgp30} is an air quality sensor that is connected to the WEMOS D1 mini as an expansion shield.
It communicates via \IIC{} to the mainboard and can report equivalent CO\textsubscript{2} measurement (in \unit{ppb}) and a total voltile organic compounds measument (in \unit{ppm}).

The interface is shown in listing~\ref{lst:aqs}.

\begin{lstlisting}[language=Clean,caption={Air quality sensor \mTask{} interface.},label={lst:aqs}]
class AirQualitySensor v where
	airqualitySensor :: AirQualitySensorInfo
		((v AirQualitySensor) -> Main (v a)) -> Main (v a) | ...
	setEnvironmentalData :: (v AirQualitySensor) (v Real) (v Real)
		-> MTask v ()
	tvoc :: (v AirQualitySensor) -> MTask v Int
	co2 :: (v AirQualitySensor) -> MTask v Int
\end{lstlisting}

As the air quality is not bound to change very quickly, the default refresh rate of airquality tasks is one minute.
To increase this for the purpose of the assignment, we use the \cleaninline{co2`} variant that takes a refresh interval parameter.

\begin{anexercise}{Air quality}{airquality0}
	The skeleton program contains an \iTask{} system that measures the equivalent CO\textsubscript{2} and places this, when it is changed, in a {SDS}.
	Typical air quality sensors need some time to warm up.
	The measurement will start at the minimum (\qty{400}{ppm}) and this will, after a thirty minutes or so to the actual measurement.
	When observing the terminal, you see that after the sensor has warmed up enough, the measurement fluctuates very quickly.
	Breathing on the sensor will also show immediate results.

	Adapt the \cleaninline{differs} function so that only values that differ more than \qty[parse-numbers=false]{\epsilon}{ppm} are reported.
	\hint{
		\cleaninline{differs} is a \Clean{} function, the host language will inline the code.
		The first argument is a \Clean{} value so you have to use \cleaninline{lit} here.
	}
\end{anexercise}

As we have seen before, the device is equipped with an \textsc{sht3x} temperature and humidity sensor.
Most air quality sensors can be made more accurate by feeding it with the current temperature and humidity using the \cleaninline{setEnvironmentalData} task.

\begin{anexercise}{Environment}{airquality1}
	Adapt the program so that it sets the environmental values before measuring the air quality.

	\hint{Use the correct sequential combinator. \cleaninline{temperature} and \cleaninline{humidity} yield unstable values so use \cleaninline{>>~.}}
\end{anexercise}

Running multiple tasks on \mTask{} devices is as simple as combining tasks with one of the parallel combinators.
Listing~\ref{lst:par} shows the types of the two parallel combinators.
Use \cleaninline{.&&.} if you want to combine the values of the two tasks.
Use \cleaninline{.||.} if you are only interested in one of the values.

\begin{lstlisting}[language=Clean,caption={Parallel task combinators in \mTask{}.},label={lst:par}]
class (.&&.) infixr 4 v :: (MTask v a) (MTask v b) -> MTask v (a, b) | ...
class (.||.) infixr 3 v :: (MTask v a) (MTask v a) -> MTask v a      | ...
\end{lstlisting}

\begin{anexercise}{Changing environments}{airquality2}
	Adapt the program so that it the environmental value is set each time either the humidity or the temperature changes.

	To give some feedback to the user that the environmental values have changed, blink the LED after setting the environmental values.
\end{anexercise}

\newpage
\appendix
\section{Solutions}
\begin{asolution}{Hello World!}
	See listings~\ref{lst:helloworldI} and~\ref{lst:helloworldM}.
\end{asolution}

\begin{asolution}{Colors}
	\lstinputlisting[language=Clean,linerange={31-42},numbers=left,basicstyle=\scriptsize\tt]{colors.icl}%chktex 8
\end{asolution}

\begin{asolution}{Walking}
	\lstinputlisting[language=Clean,linerange={25-38},numbers=left,basicstyle=\scriptsize\tt]{walking.icl}%chktex 8
\end{asolution}

\begin{asolution}{Temperature}
	\lstinputlisting[language=Clean,linerange={35-55},numbers=left,basicstyle=\scriptsize\tt]{temperature.icl}%chktex 8
\end{asolution}

\begin{asolution}{Airquality}
	\lstinputlisting[language=Clean,linerange={47-50},numbers=left,basicstyle=\scriptsize\tt]{airquality0.icl}%chktex 8
\end{asolution}

\begin{asolution}{Environment}
	\lstinputlisting[language=Clean,linerange={43-49},numbers=left,basicstyle=\scriptsize\tt]{airquality1.icl}%chktex 8
\end{asolution}

\begin{asolution}{Changing environments}
	\lstinputlisting[language=Clean,linerange={32-63},numbers=left,basicstyle=\scriptsize\tt]{airquality2.icl}%chktex 8
\end{asolution}
\end{document}
